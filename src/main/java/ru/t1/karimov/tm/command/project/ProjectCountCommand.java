package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;

public final class ProjectCountCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[PROJECT COUNT]");
        System.out.println("Count = " + getProjectService().getSize(userId));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-count";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Count projects.";
    }

}
