package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.exception.AbstractException;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name) throws AbstractException;

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
