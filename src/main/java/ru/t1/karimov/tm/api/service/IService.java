package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.IRepository;
import ru.t1.karimov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
