package ru.t1.karimov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.NameComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<Task> comparator;

    TaskSort(@NotNull final String displayName, @NotNull final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
